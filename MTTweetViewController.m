//
//  MTTweetViewController.m
//  MyTweeter
//
//  Created by Racine, James, Ernest on 2/25/14.
//  Copyright (c) 2014 Racine, James, Ernest. All rights reserved.
//

#import "MTTweetViewController.h"

@interface MTTweetViewController ()

@end

@implementation MTTweetViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initWithPage:(NSString *)page{
    self = [self initWithNibName:@"MTTweetViewController" bundle:[NSBundle mainBundle]];
    self.wPage = page;
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[_tweetPage setDelegate:self];
    [_tweetPage loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_wPage]]];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    // starting the load, show the activity indicator in the status bar
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    // finished loading, hide the activity indicator in the status bar
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}
*/
@end
