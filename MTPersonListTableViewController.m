//
//  MTPersonListTableViewController.m
//  MyTweeter
//
//  Created by Racine, James, Ernest on 2/28/14.
//  Copyright (c) 2014 Racine, James, Ernest. All rights reserved.
//

#import "MTPersonListTableViewController.h"
#import "MTTweetTableViewController.h"
#import "MTperson.h"
#import "MTModalTweet.h"
#import "MTAppDelegate.h"
#import <Social/SLRequest.h>

@interface MTPersonListTableViewController ()

@end

@implementation MTPersonListTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        self.tableView.tableFooterView = [UIView new];
    }
    return self;
}

-(id)initWithStyle:(UITableViewStyle)style personList:(NSArray*)perList{
    self = [self initWithStyle:style];
    if(self){
        _pList = [[NSArray alloc] initWithArray:perList];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    UITabBarItem *item;
    item = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemContacts tag:0];
    self.tabBarItem = item;
    UIBarButtonItem *postButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(dummy)];
    self.navigationItem.rightBarButtonItem = postButton;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)dummy{
    MTModalTweet* mt;
    mt = [[MTModalTweet alloc] init];
    [mt setDelegate:self];
    [self presentViewController:mt animated:YES completion:^(void){}];
}

-(void)closeWindow{
    [self dismissViewControllerAnimated:YES completion:^(void){}];
}

-(void)postTweet:(NSString*)tweet{
    NSMutableDictionary *ns = [[NSMutableDictionary alloc] init];
    [ns setValue:tweet forKey:@"status"];
    MTAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/update.json"];
    SLRequest *postRequest = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodPOST URL:url parameters:ns];
    [postRequest setAccount:appDelegate.account];
    [postRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse,NSError *error)
     {
         if([urlResponse statusCode] == 200)
         {
             NSError *jsonError = nil;
             id jsonResult = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&jsonError];
             if(jsonResult != nil)
             {
                 [appDelegate refreshTweets];
             }
             else
             {
                 NSString *message = [NSString stringWithFormat:@"Could not post your tweet: %@",[jsonError localizedDescription]];
                 [[[UIAlertView alloc] initWithTitle:@"Error" message:message delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil]
                  show];
             }
         }else{
             NSLog(@"%d",[urlResponse statusCode]);
         }
     }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MTTweetTableViewController *tbView;
    MTPerson *p = [[MTPerson alloc] init];
    p = [_pList objectAtIndex:indexPath.row];
    tbView = [[MTTweetTableViewController alloc] initWithNibName:@"MTTweetTableViewController" bundle:[NSBundle mainBundle] style:UITableViewStylePlain tweetList:p.tweets user:p];
    [self.navigationController pushViewController:tbView animated:YES];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [_pList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Person";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    // Configure the cell...
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    NSDictionary *item = (NSDictionary *)[self.pList objectAtIndex:indexPath.row];
    cell.textLabel.text = [item valueForKey:@"user"];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%d tweets",[[item valueForKey:@"tweets"] count]];
    NSURL *url = [NSURL URLWithString:[item valueForKey:@"photo"]];
    NSData *data = [NSData dataWithContentsOfURL:url];
    cell.imageView.image = [[UIImage alloc] initWithData:data];
    return cell;
}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
