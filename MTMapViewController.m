//
//  MTMapViewController.m
//  MyTweeter
//
//  Created by Racine, James, Ernest on 3/25/14.
//  Copyright (c) 2014 Racine, James, Ernest. All rights reserved.
//

#import "MTMapViewController.h"
#import <MapKit/MapKit.h>
#import "MTAppDelegate.h"
#import "MTTweetTableViewController.h"
#import "MTPlacemark.h"
#import "MTPerson.h"

@interface MTMapViewController ()

@end

@implementation MTMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_map setDelegate:self];
    MTAppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
    [_map removeAnnotations:[_map annotations]];
    for(MTPerson *p in [[appDelegate pDict] allValues]){
        [_map addAnnotation:p.location];
    }
    
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    MTAppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
    for(MTPerson *p in [[appDelegate pDict] allValues]){
        [_map addAnnotation:p.location];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation{
    NSString* AnnotationIdentifier = @"Annotation";
    MKPinAnnotationView *pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationIdentifier];
    if (!pinView)
    {
        pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationIdentifier];
        pinView.animatesDrop = NO;
        pinView.canShowCallout = YES;
    }
    else
    {
        pinView.annotation = annotation;
    }
    MTPlacemark *mPlace = annotation;
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    [rightButton addTarget:self action:@selector(pushTweetTable:) forControlEvents:UIControlEventTouchUpInside];
    [rightButton setTitle:[[mPlace user] handle] forState:UIControlStateNormal];
    pinView.rightCalloutAccessoryView = rightButton;
    NSURL *url = [NSURL URLWithString:[[mPlace user] valueForKey:@"photo"]];
    NSData *data = [NSData dataWithContentsOfURL:url];
    pinView.leftCalloutAccessoryView = [[UIImageView alloc] initWithImage:[[UIImage alloc] initWithData:data]];
    UILabel *l = [[UILabel alloc] init];
    l.center = [pinView.leftCalloutAccessoryView center];
    l.text = [NSString stringWithFormat:@"%d tweets",[[[mPlace user] tweets] count]];
    [pinView.leftCalloutAccessoryView addSubview:l];
    return pinView;
}

-(void)pushTweetTable:(id)sender{
    UIButton *b = sender;
    MTAppDelegate *appDel = [[UIApplication sharedApplication] delegate];
    MTPerson *p = [appDel.pDict valueForKey:[b titleForState:UIControlStateNormal]];
    MTTweetTableViewController *tbView = [[MTTweetTableViewController alloc] initWithNibName:@"MTTweetTableViewController" bundle:[NSBundle mainBundle] style:UITableViewStylePlain tweetList:p.tweets user:p];
    [self.navigationController pushViewController:tbView animated:YES];
}
@end
