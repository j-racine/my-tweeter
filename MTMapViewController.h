//
//  MTMapViewController.h
//  MyTweeter
//
//  Created by Racine, James, Ernest on 3/25/14.
//  Copyright (c) 2014 Racine, James, Ernest. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MTMapViewController : UIViewController<MKMapViewDelegate>
@property (weak, nonatomic) IBOutlet MKMapView *map;

@end
