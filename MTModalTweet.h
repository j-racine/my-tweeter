//
//  MTModalTweet.h
//  MyTweeter
//
//  Created by Racine, James, Ernest on 3/24/14.
//  Copyright (c) 2014 Racine, James, Ernest. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MTModalTweetDelegate;

@interface MTModalTweet : UIViewController


@property (nonatomic, weak) id<MTModalTweetDelegate> delegate;


@end


@protocol MTModalTweetDelegate <NSObject>

-(void)postTweet:(NSString*)tweet;

-(void)closeWindow;

@end
