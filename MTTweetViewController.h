//
//  MTTweetViewController.h
//  MyTweeter
//
//  Created by Racine, James, Ernest on 2/25/14.
//  Copyright (c) 2014 Racine, James, Ernest. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MTTweetViewController : UIViewController
@property (strong, nonatomic) NSString *wPage;
@property (weak, nonatomic) IBOutlet UIWebView *tweetPage;

-(id)initWithPage:(NSString*)page;

@end
