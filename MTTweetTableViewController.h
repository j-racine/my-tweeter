//
//  MTTweetTableViewController.h
//  MyTweeter
//
//  Created by Racine, James, Ernest on 3/3/14.
//  Copyright (c) 2014 Racine, James, Ernest. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MTPerson.h"

@interface MTTweetTableViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *fileView;
@property (strong,nonatomic) NSArray *tweetList;
@property (strong,nonatomic) UITableViewCell *myCell;
@property (weak,nonatomic) MTPerson* user;

- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil style:(UITableViewStyle) tableStyle tweetList:(NSArray *)tweets;
- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil style:(UITableViewStyle) tableStyle tweetList:(NSArray *)tweets user:(MTPerson *)p;

@end
