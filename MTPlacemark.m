//
//  MTPlacemark.m
//  MyTweeter
//
//  Created by Racine, James, Ernest on 3/26/14.
//  Copyright (c) 2014 Racine, James, Ernest. All rights reserved.
//

#import "MTPlacemark.h"

@implementation MTPlacemark

-(id)initWithPlacemark:(CLPlacemark *)placemark User:(MTPerson*)user{
    self = [super initWithPlacemark:placemark];
    if(self){
        _user = user;
        
    }
    return self;
}

@end
