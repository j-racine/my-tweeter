//
//  MTTweetListViewController.m
//  MyTweeter
//
//  Created by Racine, James, Ernest on 2/24/14.
//  Copyright (c) 2014 Racine, James, Ernest. All rights reserved.
//

#import "MTTweetListViewController.h"
#import "MTTweetViewController.h"

@interface MTTweetListViewController ()


@end

@implementation MTTweetListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initWithNibName:(NSString *)nibNameOrNil person:(NSString *)per
{
    self = [self initWithNibName:nibNameOrNil bundle:[NSBundle mainBundle]];
    if([per  isEqual: @"Sonic"]){
        self.title = @"Sonic's Tweets";
    }else{
        self.title = @"Darunia's Tweets";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //only necessary for Recents
    if([self.title isEqual:@"Sonic's Tweets"]){
        [self.tweet1 setText:@"www.gottagofast.com"];
        [self.tweet2 setText:@"www.blueblur.com"];
        [self.tweet3 setText:@"http://www.sega.com/"];
        [self.tweet4 setText:@"www.knucklesfanclub.com"];
    }else if([self.title isEqualToString:@"Darunia's Tweets"]){
        [self.tweet1 setText:@"www.sirloinrocks.com"];
        [self.tweet2 setText:@"www.hippestthebeats.com"];
        [self.tweet3 setText:@"www.goronsinc.org"];
        [self.tweet4 setText:@"http://www.nintendo.com/"];
    }else{
        [self.tweet1 setText:@"http://www.bu.edu/"];
        [self.tweet2 setText:@"http://www.twitter.com/"];
        [self.tweet3 setText:@"http://www.bbc.org/"];
        [self.tweet4 setText:@"http://www.facebook.com/"];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)viewTweet1:(id)sender {
    [self viewTweet:_tweet1.text];
}
- (IBAction)viewTweet2:(id)sender {
    [self viewTweet:_tweet2.text];
}
- (IBAction)viewTweet3:(id)sender {
    [self viewTweet:_tweet3.text];
}
- (IBAction)viewTweet4:(id)sender {
    [self viewTweet:_tweet4.text];
}

-(void)viewTweet:(NSString*)url{
    MTTweetViewController *tView;
    tView = [[MTTweetViewController alloc] initWithPage:url];
    [self.navigationController pushViewController:tView animated:YES];
}

@end
