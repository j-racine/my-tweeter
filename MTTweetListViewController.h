//
//  MTTweetListViewController.h
//  MyTweeter
//
//  Created by Racine, James, Ernest on 2/24/14.
//  Copyright (c) 2014 Racine, James, Ernest. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MTTweetListViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *tweet1;
@property (weak, nonatomic) IBOutlet UILabel *tweet2;
@property (weak, nonatomic) IBOutlet UILabel *tweet3;
@property (weak, nonatomic) IBOutlet UILabel *tweet4;

-(id)initWithNibName:(NSString *)nibNameOrNil person:(NSString *)per;

@end
