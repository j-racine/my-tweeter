//
//  MTAppDelegate.m
//  MyTweeter
//
//  Created by Racine, James, Ernest on 2/24/14.
//  Copyright (c) 2014 Racine, James, Ernest. All rights reserved.
//

#import "MTAppDelegate.h"
#import "MTPersonListTableViewController.h"
#import "MTTweetTableViewController.h"
#import "MTTweet.h"
#import "MTPerson.h"
#import "MTPlacemark.h"
#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CLGeocoder.h>
#import <Social/SLRequest.h>

@implementation MTAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    _tbCon = [[UITabBarController alloc] init];
    _contactsCon = [[UINavigationController alloc] init];
    _recentsCon = [[UINavigationController alloc] init];
    _mapCon = [[UINavigationController alloc] init];
    _pDict = [[NSMutableDictionary alloc] init];
    _tweets = [[NSMutableDictionary alloc] init];
    
    

    /*
     Given code snippet--conneecting to twitter
    */
     if(_account == nil)
     {
        if(_accountStore == nil)
        {
            self.accountStore = [[ACAccountStore alloc] init];
        }
        
        ACAccountType *accountTypeTwitter = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
        [self.accountStore requestAccessToAccountsWithType:accountTypeTwitter options:nil completion:^(BOOL granted, NSError *error)
            {
                    if(granted)
                    {
                        dispatch_sync(dispatch_get_main_queue(),^{
                            self.account = [self.accountStore accountsWithAccountType:accountTypeTwitter][0];
                            //updates the data stores and table veiws ie [self refreshData];
                            //[self refreshTweets];
                            //[self initializeScreen];
                            [self initializeTweets];
                        });
                    }
                }
             ];
     }
    
    
     
     //Given code snippet -- getting the home timeline
    
    
    
    return YES;
    
    
    
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    _tweets = nil;
    _pDict = nil;
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void)initializeTweets{
    NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/home_timeline.json?count=200"];
    SLRequest *postRequest = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodGET URL:url parameters:nil];
    [postRequest setAccount:_account];
    
    [postRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse,NSError *error)
     {
         if([urlResponse statusCode] == 200)
         {
             NSError *jsonError = nil;
             id jsonResult = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&jsonError];
             if(jsonResult != nil)
             {
                 dispatch_sync(dispatch_get_main_queue(), ^{
                     //here, update your model using jsonResult
                     MTTweet *t;
                     for(NSDictionary *n in jsonResult){
                         t = [[MTTweet alloc] init];
                         t = [_tweets valueForKey:[n valueForKey:@"id_str"]];
                         if(t == nil){
                             NSString *name = [[n valueForKey:@"user"] valueForKey:@"screen_name"];
                             MTPerson *p = [[MTPerson alloc] init];
                             p = [_pDict objectForKey:name];
                             if(p == nil){
                                 p = [[MTPerson alloc] initWithUser:[[n valueForKey:@"user"] valueForKey:@"name"] Photo:[[n valueForKey:@"user"] valueForKey:@"profile_image_url"] SN:name];
                                 NSString *l = [[n valueForKey:@"user"] valueForKey:@"location"];
                                 CLGeocoder *geo = [[CLGeocoder alloc] init];
                                     [geo geocodeAddressString:l completionHandler:^(NSArray* placemarks, NSError *err){
                                         NSLog([err debugDescription]);
                                         CLPlacemark *place = (CLPlacemark*)[placemarks firstObject];
                                         MTPlacemark *mPlace = [[MTPlacemark alloc] initWithPlacemark:place User:p];
                                         [p setLocation:mPlace];
                                     }];
                                     [_pDict setValue:p forKey:name];
                             }
                             t = [[MTTweet alloc] initWithUser:p Text:[n valueForKey:@"text"] Date:[n valueForKey:@"created_at"] ID:[n valueForKey:@"id_str"]];
                             [_tweets setValue:t forKey:t.id_string];
                             [p addTweet:t];
                         }
                     }
                     [self initializeScreen];
                 });
             }
             else
             {
                 NSString *message = [NSString stringWithFormat:@"Could not parse your timeline: %@",[jsonError localizedDescription]];
                 [[[UIAlertView alloc] initWithTitle:@"Error" message:message delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil]
                  show];
             }
         }else{
             NSLog(@"%d",[urlResponse statusCode]);
         }
     }];
    
}

-(void)refreshTweets{
    NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/home_timeline.json"];
    SLRequest *postRequest = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodGET URL:url parameters:nil];
    [postRequest setAccount:_account];
    
    [postRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse,NSError *error)
     {
         if([urlResponse statusCode] == 200)
         {
             NSError *jsonError = nil;
             id jsonResult = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&jsonError];
             if(jsonResult != nil)
             {
                 dispatch_sync(dispatch_get_main_queue(), ^{
                     //here, update your model using jsonResult
                     MTTweet *t;
                     for(NSDictionary *n in jsonResult){
                         t = [[MTTweet alloc] init];
                         t = [_tweets valueForKey:[n valueForKey:@"id_str"]];
                         if(t == nil){
                             NSString *name = [[n valueForKey:@"user"] valueForKey:@"screen_name"];
                             MTPerson *p = [[MTPerson alloc] init];
                             p = [_pDict objectForKey:name];
                             if(p == nil){
                                 p = [[MTPerson alloc] initWithUser:[[n valueForKey:@"user"] valueForKey:@"name"] Photo:[[n valueForKey:@"user"] valueForKey:@"profile_image_url"]  SN:name];
                                 NSString *l = [[n valueForKey:@"user"] valueForKey:@"location"];
                                CLGeocoder *geo = [[CLGeocoder alloc] init];
                                 [geo geocodeAddressString:l completionHandler:^(NSArray* placemarks, NSError *err){
                                    NSLog([err debugDescription]);
                                    CLPlacemark *place = (CLPlacemark*)[placemarks firstObject];
                                    MTPlacemark *mPlace = [[MTPlacemark alloc] initWithPlacemark:place User:p];
                                    [p setLocation:mPlace];
                                }];
                                [_pDict setValue:p forKey:name];
                             }
                             t = [[MTTweet alloc] initWithUser:p Text:[n valueForKey:@"text"] Date:[n valueForKey:@"created_at"] ID:[n valueForKey:@"id_str"]];
                             [_tweets setValue:t forKey:t.id_string];
                             [p addTweet:t];
                         }
                     }
                     [[[self plistCon] tableView] reloadData];
                 });
             }
             else
             {
                 NSString *message = [NSString stringWithFormat:@"Could not parse your timeline: %@",[jsonError localizedDescription]];
                 [[[UIAlertView alloc] initWithTitle:@"Error" message:message delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil]
                  show];
             }
         }else{
             NSLog(@"%d",[urlResponse statusCode]);
         }
     }];
    
}

-(void)refreshTweetsForTable:(MTTweetTableViewController *)tt{
    NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/home_timeline.json"];
    SLRequest *postRequest = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodGET URL:url parameters:nil];
    [postRequest setAccount:_account];
    [postRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse,NSError *error)
     {
         if([urlResponse statusCode] == 200)
         {
             NSError *jsonError = nil;
             id jsonResult = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&jsonError];
             if(jsonResult != nil)
             {
                 dispatch_sync(dispatch_get_main_queue(), ^{
                     //here, update your model using jsonResult
                     MTTweet *t;
                     for(NSDictionary *n in jsonResult){
                         t = [[MTTweet alloc] init];
                         t = [_tweets valueForKey:[n valueForKey:@"id_str"]];
                         if(t == nil){
                             NSString *name = [[n valueForKey:@"user"] valueForKey:@"screen_name"];
                             MTPerson *p = [[MTPerson alloc] init];
                             p = [_pDict objectForKey:name];
                             if(p == nil){
                                 p = [[MTPerson alloc] initWithUser:[[n valueForKey:@"user"] valueForKey:@"name"] Photo:[[n valueForKey:@"user"] valueForKey:@"profile_image_url"]  SN:name];
                                 NSString *l = [[n valueForKey:@"user"] valueForKey:@"location"];
                                 CLGeocoder *geo = [[CLGeocoder alloc] init];
                                     [geo geocodeAddressString:l completionHandler:^(NSArray* placemarks, NSError *err){
                                         NSLog([err debugDescription]);
                                         CLPlacemark *place = (CLPlacemark*)[placemarks firstObject];
                                         MTPlacemark *mPlace = [[MTPlacemark alloc] initWithPlacemark:place User:p];
                                         [p setLocation:mPlace];
                                     }];
                                     [_pDict setValue:p forKey:name];
                             }
                             t = [[MTTweet alloc] initWithUser:p Text:[n valueForKey:@"text"] Date:[n valueForKey:@"created_at"] ID:[n valueForKey:@"id_str"]];
                             [_tweets setValue:t forKey:t.id_string];
                             [p addTweet:t];
                         }
                     }
                     
                     if(tt.user == nil)
                         tt.tweetList = [[[self tweets] allValues] sortedArrayUsingComparator:^(id obj1,id obj2){
                             return [[obj1 valueForKey:@"date"] localizedCompare:[obj2 valueForKey:@"date"]];
                         }];
                     else
                         tt.tweetList = [[[self pDict] valueForKey:[tt.user handle]] tweets];
                     [[self plistCon] setPList:[[self pDict] allValues]];
                     [[tt tableView] reloadData];
                     [[[self plistCon] tableView] reloadData];
                 });
             }
             else
             {
                 NSString *message = [NSString stringWithFormat:@"Could not parse your timeline: %@",[jsonError localizedDescription]];
                 [[[UIAlertView alloc] initWithTitle:@"Error" message:message delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil]
                  show];
             }
         }else{
             NSLog(@"%d",[urlResponse statusCode]);
         }
     }];
    
}


-(void)initializeScreen{
    _plistCon = [[MTPersonListTableViewController alloc] initWithStyle:UITableViewStylePlain personList:[_pDict allValues]];
    _plistCon.title = @"Contacts";
    
    _rListCon = [[MTTweetTableViewController alloc] initWithNibName:@"MTTweetTableViewController" bundle:[NSBundle mainBundle]];
    _rListCon.title = @"Recents";
    _rListCon.tabBarItem = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemRecents tag:0];
    _rListCon.tweetList = [NSArray arrayWithArray:[[_tweets allValues] sortedArrayUsingComparator:^(id obj1,id obj2){
        return [[obj1 valueForKey:@"date"] localizedCompare:[obj2 valueForKey:@"date"]];
    }]];
    _map = [[MTMapViewController alloc] initWithNibName:@"MTMapViewController" bundle:[NSBundle mainBundle]];
    _map.title = @"Map";
    _map.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Map" image:Nil tag:1];
    [_map.map setDelegate:_map];
    
    [_contactsCon pushViewController:_plistCon animated:YES];
    [_recentsCon pushViewController:_rListCon animated:YES];
    [_mapCon pushViewController:_map animated:YES];
    _tbCon.viewControllers = [[NSMutableArray alloc] initWithObjects:_contactsCon,_recentsCon,_mapCon, nil];
    _window.rootViewController = _tbCon;
}

@end
