//
//  MTAppDelegate.h
//  MyTweeter
//
//  Created by Racine, James, Ernest on 2/24/14.
//  Copyright (c) 2014 Racine, James, Ernest. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/SLRequest.h>
#import "MTPersonListTableViewController.h"
#import "MTTweetTableViewController.h"
#import "MTMapViewController.h"
#import <Accounts/Accounts.h>

#import <Social/SLRequest.h>

@interface MTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UITabBarController *tbCon;
@property (strong, nonatomic) UINavigationController *contactsCon;
@property (strong, nonatomic) UINavigationController *recentsCon;
@property (strong, nonatomic) UINavigationController *mapCon;
@property (strong, nonatomic) MTMapViewController *map;
@property (strong, nonatomic) MTPersonListTableViewController *plistCon;
@property (strong, nonatomic) MTTweetTableViewController *rListCon;
@property (strong, nonatomic) NSMutableDictionary *pDict;
@property (strong,nonatomic) ACAccount *account;
@property (strong,nonatomic) ACAccountStore *accountStore;
@property (strong,nonatomic) NSMutableDictionary *tweets;

-(void)refreshTweetsForTable:(MTTweetTableViewController*)tt;
-(void)refreshTweets;
@end
