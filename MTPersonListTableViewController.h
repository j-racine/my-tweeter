//
//  MTPersonListTableViewController.h
//  MyTweeter
//
//  Created by Racine, James, Ernest on 2/28/14.
//  Copyright (c) 2014 Racine, James, Ernest. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MTModalTweet.h"

@interface MTPersonListTableViewController : UITableViewController<MTModalTweetDelegate>

@property (strong, nonatomic) NSArray *pList;

-(id)initWithStyle:(UITableViewStyle)style personList:(NSArray*)perList;

@end
